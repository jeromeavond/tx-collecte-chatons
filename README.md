# Tx - C.H.A.T.O.N.S.

Ce projet vise à définir un modèle permettant aux différents CHATONS de décrire ce qu'ils sont et ce qu'ils font, afin de :
- Faciliter la maintenance de ces informations,
- Proposer une approche décentralisée de cette description,
- Permettre à un outil de recherche de compiler ces informations et proposer à un⋅e utilisateurice des résultats pertinents et à jour.

Ce projet de Tx n'a pas vocation à imposer un format aux CHATONS, simplement à proposer une des multiples approches. Ainsi, ce format est amené à évoluer, et à se faire approprier ou pas par les CHATONS.


## Le schéma
L'approche du JSON validé par un schéma aura été choisie. Chaque CHATONS propose un fichier JSON, valide selon le schéma présenté, qui contient les principales informations telles que :
- `url` : le site du CHATONS,
- `name` : le nom
- `servers` : la liste des hébergeurs,
- `address` : l'adresse du CHATONS,
- `status` : l'état actuel du CHATONS,
- `public` : le public visé,
- `structure` : la structure du CHATONS (association, entreprise, …)
- `economic_model` : les modèles économiques,
- `services` : la liste des services proposés.


## Le crawler
Le but du crawler est de télécharger tous les fichiers JSON répertoriés et de les exporter en un format pertinent pour une utilisation ultérieure. Il reste encore à définir précisément quel est ce format.

## La liste des URL à vérifier
Le crawler prend en entrée une liste d'URL à télécharger. Les merge request sont les bienvenues sur cette liste pour intégrer de nouveaux CHATONS à l'outil de centralisation.

## Le site
Un site a été mis en place pour présenter la démarche et mettre à disposition les résultats de la collecte. Ses sources sont aussi sur ce dépôt.
