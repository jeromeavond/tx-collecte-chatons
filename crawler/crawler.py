#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ./crawler.py -s ../schema/CHATONS_schema.json -i ../liste/CHATONS_list.txt

import os
import sys
import argparse
from jsonschema import validate, ValidationError, SchemaError
import json
import json_chaton
import logger

# Ouverture du fichier de log
dir = os.path.dirname(os.path.realpath(__file__))
log = logger.Log(os.path.join(dir, 'log.txt'))
log.log("Begin session : " + ' '.join(sys.argv))

# Définition des arguments
parser = argparse.ArgumentParser(formatter_class=argparse.RawTextHelpFormatter)
parser.add_argument(
    '-u', "--update-only", action='store_true',
    required=False,
    help="Ne permet pas la supression d'un CHATON d'une exécution sur l'autre,\
    \nles anciennes données sont conservées.\n Seuls l'ajout et la \
    modification sont présents dans le fichier mis à jour.")
parser.add_argument(
    '-v', "--verbose", action='store_true', required=False,
    help="Affiche plus d'informations")
parser.add_argument(
    '-o', "--output", type=argparse.FileType('w'), required=False,
    help="Fichier JSON dans lequel exporter les données collectées\n \
    (Par défaut : INPUT.json)")
parser.add_argument(
    '-s', "--schema", type=argparse.FileType('r'), required=False,
    help="Schéma JSON auquel confronter les données si donné en argument")
parser.add_argument(
    '-i', "--input", type=argparse.FileType('r'), required=True,
    help="Liste des adresses des fichiers JSON décrivant les CHATONS")

# Lecture des arguments I/O
try:
    args = parser.parse_args()
except SystemExit:
    exc = sys.exc_info()[1]
    # print(exc)
    log.error("Invalid arguments", 0, True)
    log.log("End session")
    exit(0)

# Attention aux valeurs par défaut, respecter la structure des répertoires
json_input_file = args.input.name
json_output_file = args.output.name if args.output \
    else os.path.splitext(json_input_file)[0] + '.json'
json_schema = args.schema.name if args.schema else None

# Ouverture du fichier d'entrée donné en argument
with open(json_input_file) as list_file:
    url_list = list_file.read().split('\n')

# Ouverture du fichier de sortie donné en argument
input_old = None
try:
    output_file = open(json_output_file)
    input_old = json.loads(output_file.read())
except (FileNotFoundError, json.JSONDecodeError):
    # On considère qu'il n'y a pas d'ancien fichier
    pass

output_file = open(json_output_file, 'w', encoding='utf8')

# Ouverture du fichier de schéma donné en argument
key = None
if json_schema:
    with open(json_schema) as schema_file:
        try:
            schema = json.loads(schema_file.read())
        except json.decoder.JSONDecodeError:
            log.error("Ill-formed JSON schema")
            log.info("List not parsed")
            log.log("End session")
            exit(0)
        else:
            try:
                key = schema['required'][0]
            except (KeyError, IndexError):
                pass
# log.debug("Key is : " + str(key))

# Préparation du méga JSON
JSON_list = []

# Boucle sur les URL trouvées
nb_total = 0
for url in url_list:
    url = url.lstrip()             # Suppression caractères blanc à gauche
    if not url or url[0] == '#':   # Ligne vide ou commance par '#' on ignore
        continue                   # Permet d'avoir des urls contenant '#'

    nb_total += 1
    log.info("Doing : " + url)

    try:
        data = json_chaton.urlToJSON(url)
    except json_chaton.NormalizeException:
        log.urlNotParsed("Problem normalizing url", 1)
        continue
    except json_chaton.UrlOpeningException:
        log.urlNotParsed("Problem opening url", 1)
        continue
    except json.JSONDecodeError:
        log.urlNotParsed("Ill-formed JSON file", 1)
        continue

    if (data is None):
        log.urlNotParsed("No data in JSON", 1)
        continue

    if json_schema:
        try:
            validate(instance=data, schema=schema)
        except ValidationError:
            log.urlNotParsed("Invalid JSON according to given schema", 1)
            continue
        except SchemaError:
            log.debug("Should not happen : validating schema")
            continue

    if json_chaton.chatonIsIn(data, JSON_list):
        log.urlNotParsed("Chaton already exists", 1)
        continue

    JSON_list.append(data)
    log.info("Done", 1)
print("")
log.info("Parsed %i/%i url(s)" % (len(JSON_list), nb_total))

# Affichage du changelog
print("")
removed = []
if input_old:
    # Recherche des Chatons supprimés
    removed = json_chaton.getChatonDiff(input_old, JSON_list, key)
    if not args.update_only:
        json_chaton.showChatonListIfVerbose(
            "Removed", removed, log, args.verbose)
    # Recherche des Chatons nouveaux, mis à jour, intouchés
    n, u, t = json_chaton.getNewUpdatedAndUntouched(input_old, JSON_list, key)
    json_chaton.showChatonListIfVerbose("Added", n, log, args.verbose)
    json_chaton.showChatonListIfVerbose("Updated", u, log, args.verbose)
    json_chaton.showChatonListIfVerbose("Untouched", t, log, args.verbose)
else:
    json_chaton.showChatonListIfVerbose("Added", JSON_list, log, args.verbose)

# Ajout des anciens chatons si besoin
if args.update_only and removed:
    print("")
    print("- There are removed entries, they will added in the new file")
    json_chaton.showChatonListIfVerbose("Kept", removed, log, args.verbose)
    if not json_schema:
        JSON_list = removed + JSON_list
    else:
        # Validation des anciens Chatons par le schema
        v, i = json_chaton.validateOld(removed, schema)
        json_chaton.showChatonListIfVerbose("Valid", v, log, args.verbose)
        json_chaton.showChatonListIfVerbose("Invalid", i, log, args.verbose)
        JSON_list = v + JSON_list

# Ecriture dans le meta-fichier
print("")
log.info("Writing in %s" % json_output_file)
json.dump(JSON_list, output_file, indent=2, ensure_ascii=False)
output_file.close()
log.info("Done", 1)

log.log("End session")
