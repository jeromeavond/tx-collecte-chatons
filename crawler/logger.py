#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from datetime import datetime


class Log:
    def __init__(self, path):
        self.logfile = open(path, 'a', encoding='utf8')

    def __del__(self):
        self.logfile.close()

    def _date(self):
        now = datetime.now()
        return now.strftime("[%Y/%m/%d-%H:%M:%S]")

    def _write(self, text):
        self.logfile.write(self._date() + " " + text + '\n')

    def _show(self, text, tab=0):
        print(" "*2*tab + text)

    def log(self, text):
        self._write("  [LOG] " + text)

    def info(self, text, tab=0):
        self._write(" [INFO] " + text)
        self._show("- " + text, tab)

    def infoVerbose(self, text, verbosity, tab=0):
        self._write(" [INFO] " + text)
        if verbosity:
            self._show("- " + text, tab)

    def error(self, text, tab=0, onlyLog=False):
        self._write("[ERROR] " + text)
        if not onlyLog:
            self._show("! " + text, tab)

    def debug(self, text, tab=0):
        self._write("[DEBUG] " + text)
        self._show("# " + text, tab)

    def urlNotParsed(self, text, tab=0):
        self.error("URL not parsed : " + text, tab)
