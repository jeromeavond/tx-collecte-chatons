#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import json
import urllib.request as req
from url_normalize import url_normalize
from jsonschema import validate, ValidationError
import logger


class NormalizeException(Exception):
    pass


class UrlOpeningException(Exception):
    pass


def urlToJSON(url: str):
    """Copy the json contained in an url
    Returns a list or  dictionary
    """
    # Tentative de normalisation de l'URL
    try:
        pretty_url = url_normalize(url)
    except:
        raise NormalizeException()

    # Tentative d'ouverture du fichier pointé
    try:
        ans = req.urlopen(pretty_url)
    except:
        raise UrlOpeningException()

    # Tentative de lecture du fichier téléchargé
    data = json.loads(ans.read().decode("utf-8"))
    return data


def chatonIsIn(chaton: dict, liste: list, key: str = None) -> bool:
    # print("# Entering chatonIsIn")
    # print("Testing")
    # showChaton(chaton)
    for elmt in liste:
        # print("Versus")
        # showChaton(elmt)
        # print("# " + str(elmt))
        if key:
            try:
                if chaton[key] == elmt[key]:
                    return True
            except KeyError:
                print("# Key in chatonIsIn")
                continue
                # Arrive si l'ancien chaton n'a pas le champ clé du nouveau
        else:
            if chaton == elmt:
                return True
    return False


def getChatonDiff(old: list, new: list, key: str = None) -> list:
    """Returns old minus new"""
    # print("# Entering getChatonDiff")
    removed_list = []
    for chaton in old:
        if not chatonIsIn(chaton, new, key):
            # print("ADDED : ")
            # showChaton(chaton)
            removed_list.append(chaton)
    return removed_list


# Classe les nouveaux chatons par rapport au précédent fichier
def getNewUpdatedAndUntouched(old: list, new: list, key: str) \
        -> (list, list, list):
    newL = []
    updated = []
    untouched = []
    for chaton in new:
        is_in = False
        for candidate in old:
            if key:
                try:
                    if chaton[key] == candidate[key]:
                        is_in = True
                        if chaton != candidate:
                            updated.append(chaton)
                        else:
                            untouched.append(chaton)
                        break
                except KeyError:
                    print("# Key in getNewUpdatedAndUntouched")
                    continue
                    # idem
            else:
                if chaton == candidate:
                    is_in = True
                    untouched.append(chaton)
                    break
        if not is_in:
            newL.append(chaton)
    return newL, updated, untouched


def showChatonListIfVerbose(
        msg: str, chatons: list,
        log: logger.Log = None,
        verbosity: bool = False):
    nb = len(chatons)
    if nb > 0:
        log.info(msg + ' : ' + str(nb))
        for chaton in chatons:
            # log.debug(str(chaton))
            try:
                name = chaton['name']
            except KeyError:
                name = "Unknown name"
            if type(log) is logger.Log:
                log.infoVerbose(name, verbosity, 1)
            elif verbosity:
                print("  - " + name)


def showChaton(chaton):  # debug
    try:
        print("C : " + chaton['name'])
    except KeyError:
        print("C : Unknown name")


# Pour valider les anciens chatons selon l'actuel schéma si besoin
def validateOld(chatons: list, schema) -> (list, list):
    validL = []
    invalidL = []
    for chaton in chatons:
        try:
            validate(instance=chaton, schema=schema)
        except ValidationError:
            invalidL.append(chaton)
        else:
            validL.append(chaton)
    return validL, invalidL
