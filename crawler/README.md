# Principe du crawler
Dans un fichier  `liste.txt`, il y a une liste des chemins vers les fichiers json qui décrivent les différents Chatons. Cette liste est aussi simple que possible, il y a une url par ligne.
Les fichiers pointés sont tous téléchargés par le script, puis agglomérés en un json plus gros. Cet export est amené à évoluer à mesure que nous trouvons des solutions plus pertinentes.


# Dépendances
À installer avec pip :
- `url_normalize`
- `jsonschema`


# Utilisation
Le crawler s'appelle en ligne de commande en précisant le fichier d'entrée, celui de sortie et celui du schéma.
```
crawler.py [-h] [-u] [-v] [-o OUTPUT] [-s SCHEMA] -i INPUT
```
- `-i, --input INPUT` : Fichier texte contenant une liste d'urls pointant vers les jsons des Chatons
- `-o, --output OUTPUT` : Fichier json dans lequel sont exportés toutes les informations récoltées. Par défaut il prend le nom `INPUT.json`
- `-s, --schema SCHEMA` : Fichier optionnel, si donné les jsons des Chatons doivent être valides pour être ajoutés à `OUTPUT`
- `-v, --verbose` : Affiche plus d'informations dans la console
- `-u, --update-only` : Permet de garder les Chatons qui seraient supprimés entre deux exécutions successives (par exemple si un Chaton a un problème avec son json pendant la procédure de mise à jour).
Pour comparer deux Chatons (pour savoir qui a été ajouté, mis à jour, supprimé, ...), s'il y n'y a pas de schema, c'est la totalité des informations qui sont comparées entre elles, et si un schema est proposé, le crawler essaie d'utiliser comme clé primaire le premier champ requis dans le schema. Dans le schema actuel, cette clé a la valeur `url` : deux fichier json sont donc la description du même Chaton si leurs urls sont les mêmes.

Nous recommandons de le lancer ainsi :
```
./crawler.py -u -v -o sortie/chatons.json -s ../schema/CHATONS_schema.json -i ../liste/CHATONS_list.txt
```
