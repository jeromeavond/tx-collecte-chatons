# Principe du schéma
Le schéma CHATONS\_schema.json a vocation à valider que la fiche d'un CHATONS contient les bonnes informations. Pour l'instant, il permet de valider un document JSON « simple » et pas JSON-LD, c'est une piste possible d'amélioration.


# Définition des champs
Dans ce système clé/valeur, pour correspondre au web sémantique les clés sont exprimées en anglais, et les valeurs sont en français, dans une liste de valeurs prédéfinies en général.

La question de comment décider d'ajouter une valeur à cette liste valide pour étendre le modèle sans en compromettre l'intégrité est à se poser.

Sur la base des repérages faits au début du projet, nous avons pu établir le modèle suivant :

```plantuml
hide circle


class c as "CHATONS" {
  # url : String
  - name : String
  - description : String
  - creation_date : String
  - address : String
  - perimeter : String
  - structure : String
  - status : String
  - public : [String]
  - economic_model : [String]
  - servers : [Sring]
  - services : [Service]
}

class S as "Service" {
  - type : String
  - url : String
  - software : String
  - account : String
  - price : String
  - degradability : Public
}
```

## Champs obligatoires
Sur le site actuel du collectif, les champs obligatoires sont le **nom**, la **structure** et le **statut**. Nous partons donc de cette base et rendons aussi obligatoire l'**URL**, qui servira d'identifiant.

Pour l'instant, les noms des services sont contraints, ce qui permet de s'assurer de la cohérence dans la dénomination d'un même service. Néanmoins, cela devrait peut-être être assoupli puisque le site actuel des CHATONS permet de saisir un service personnalisé. En tout cas, les merge request sont les bienvenues pour enrichir la liste de services.

Dans des versions futures, il sera sans doute intéressant de rendre obligatoires le public (au niveau du CHATONS) et le modèle économique (idem). Idem, on pourrait rendre obligatoire l'URL des services pour avoir une base de données qui permette d'alimenter entraide.chatons.org.

L'adresse est peut-être plus délicate puisqu'un CHATONS peut avoir un siège à un endroit et des serveurs à un autre. Idem pour l'hébergeur, qui n'est sans doute pas pertinent si le CHATONS est auto-hébergé.

Enfin, il ne devrait pas être obligatoire de renseigner le public et le tarif pour chaque service, sous peine de devenir trop lourd.

## Détails des champs d'un CHATONS
### url
C'est l'adresse web du CHATONS.

### description
Une courte présentation de la structure et de ses activités.

### creation_date
La date de création du CHATONS, au format "AAAA", "AAAA-MM" ou "AAAA-MM-JJ".

### address
L'adresse de son siège.

### perimeter
Pour renseigner le périmètre des actions du CHATONS, par exemple la zone géographique à laquelle il propose préférentiellement ses services ou ses autres activités.

### structure
La forme juridique du CHATONS. Elle prend une valeur parmi une liste déjà définie :
- "association" : pour les associations loi 1901 ;
- "collectif" : pour les groupes informels ;
- "cooperative" : pour les SCOP et autres entreprises coopératives ;
- "micro\_entreprise" : pour les entreprises de moins de 10 personnes, à l'exception de celles d'une seule ;
- "entreprise" : pour les autres entreprises ;
- "particulier" : pour les projets individuels ;
- "auto-entrepreneur.euse" : pour les projets individuels à but lucratif ou sous statut d'entreprise ;
- "autre" : pour ce qui ne rentre pas dans une des catégories énoncées plus haut.

### economic_model
Comment le CHATONS se finance. Il est possible de cumuler plusieurs valeurs parmi :
- "gratuit" : certains services sont gratuits ;
- "payant" : certains services sont payants ;
- "prix\_libre" : certains services sont à prix libre ;
- "freemium" : certains services sont en freemium ;
- "adhesion" : la structure demande des adhésions à ses membres (certains services peuvent être réservés aux adhérent.e.s) ;
- "don" : la structure accepte les dons ou en reçoit beaucoup ;
- "subvention" : la structure est subventionnée ;
- "autre".

### public
Qui peut utiliser les services du CHATONS. Comme chaque service peut avoir un public différent, il est possible de choisir plusieurs valeurs.
- "tou.te.s" : il n'y a aucun critère pour utiliser certains services ;
- "adheren.te.s" : certains services sont réservés aux adhérent.e.s ;
- "client.e.s" : certains services ne peuvent être utilisés que par celles et ceux qui le paient ;
- "cercle\_prive" : certains services ne sont accessibles que sur cooptation.

### servers
La liste des URL des sites des hébergeurs du CHATONS.

### status
C'est le statut du CHATONS dans le collectif, il peut être :
- "non\_verifie" ;
- "evaluation\_en\_cours" ;
- "approuve" ;
- "conflit\_potentiel" ;
- "mediation\_demandee" ;
- "retire".

## Détail des champs d'un service
### type
C'est le nom du service, ce qu'il permet de faire. Pour la liste complète, voir directement dans le schéma.

Cette liste a été tirée du site chatons.org et contient quelques subtilités. Par exemple, "messagerie\_instantanee" fait référence à des messageries de type Slack, avec une information organisée, alors que "chat" recouvre des services qui ressemblent à MSN, plus orientés vers la discussion personne à personne et sans structuration des discussions. Pour plus de détails, se référer au site du collectif.

### software
Le logiciel qui rend ce service. Les valeurs possibles viennent aussi du site des CHATONS et peuvent se trouver dans le fichier du schéma.

### url
Un lien d'accès direct au service.

### price
Si le service est payant ou gratuit. Cette information peut servir à construire le modèle économique du CHATONS. Les valeurs possibles sont :
- "gratuit" ;
- "payant" ;
- "prix\_libre" ;
- "freemium" : certaines fonctionnalités sont accessibles à tou.te.s et d'autres nécessitent de payer.

### account
S'il est nécessaire de créer ou pas un compte.
- "sans\_inscription" ;
- "sur\_inscription" ;
- "restreint" : non seulement il faut créer un compte, mais l'ouverture de compte est réservée à certaines personnes (adhérent.e.s, ami.e.s…).

### degradability
Pour combien de temps les données sont gardées (sur un pad ou un hébergement d'images par exemple) :
- "1\_jour" ;
- "1\_semaine" ;
- "1\_mois" ;
- "2\_mois" ;
- "6\_mois" ;
- "1\_an" ;
- "toujours".
