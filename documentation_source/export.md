Différentes solutions possibles pour interroger les données collectées
======================================================================

Ce site propose de manière statique de télécharger le JSON de tous les
Chatons. Il se trouve [ici](http://chatons.picasoft.net/exports/chatons.json).

Il est également possible d'interroger ce fichier
pour trouver un Chaton ou un service spécifique, par exemple, et de
demander des exports particuliers. Pour ce faire, plusieurs possibilités
se présentaient, et notamment plusieurs choix d'architecture.

Une architecture avec serveur applicatif seulement
--------------------------------------------------

C'est la solution que nous avons choisie.

Nous avons développé [une page](http://chatons.picasoft.net/search/recherche.html)
qui charge le JSON dans le navigateur de l'utilisateur·ice et lui permet
d'effectuer une recherche. Pour l'instant, il n'est possible de chercher un
Chaton que sur la base de sa structure, des services qu'il propose, de son
modèle économique et du public auquel il s'adresse.

Une architecture serveur applicatif et serveur de données
---------------------------------------------------------

Ici, il s'agit de créer une base de données, d'entrer les informations
dedans et de développer une application qui interroge cette base de
données.

Nous n'avons pas retenu cette solution parce qu'elle ajoute une couche
de complexité au projet alors que les avantages des bases de données
(notamment la performance avec des gros volumes de données) ne sont pas
utiles ici.

Nous avions considéré comme candidats les SGBD suivants :

### MongoDB

Avantages

-   C'est un SGDB orienté document, il prend des JSON en entrée. Il
    n'y a donc pas besoin de traiter plus avant les fichiers collectés,
    on peut directement les importer dans la BDD. Il y a juste à écrire
    des procédures de mise à jour qui préservent la cohérence de
    l'information.

Inconvénients

-   C'est un système assez lourd et gourmand, il n'y a sans doute pas
    besoin de cet outil pour un usage aussi léger.
-   Il n'est pas utilisé dans l'écosystème technique actuel du
    collectif des CHATONS, c'est donc un investissement en temps et
    compétences qui n'est pas négligeable.

### PostgreSQL

Avantages

-   Le site actuel du CHATONS utilise une BDD PostgreSQL, cela ne
    duplique pas l'infrastructure technique.

Inconvénients

-   C'est une BDD relationnelle. Les colonnes peuvent facilement avoir
    un type JSON mais il sera sans doute préférable de travailler un peu
    l'export pour en faire quelque chose de plus dans la philosophie
    des tables.

Question de l'intégration technique et fonctionnelle au site du CHATONS
=========================================================================

Pour une preuve de concept, ce site en stand-alone suffit. Cela étant,
le but à terme est d'utiliser ce mode de récolte des données pour
nourrir le site actuel du CHATONS. L'information devrait être
regroupée et pas dupliquée, et certains membres du collectif préféreront
toujours remplir le formulaire pour créer leur fiche.

Le site [entraide.chatons.org](https://entraide.chatons.org) prend en
entrée un fichier JSON assez simple avec les informations des différents
services. Il sera possible de générer un fichier qui respecte le bon
format et de le fusionner avec celui généré par le formulaire du site
chatons.org.

Pour les fiches des Chatons, c'est plus compliqué. Certains champs ne
sont pour l'instant pas intégrés dans notre schéma et seraient lourds à
inclure (le respect de la charte point par point par exemple). Il faudra
donc trouver à la fois des solutions pour faire l'interface entre les
clefs et les valeurs des deux modèles, et pour importer les données dans
le Drupal de chatons.org.

[Retour à l'accueil](index.md)
