Construire la liste des Chatons
===============================

Le principe du crawler et l'intérêt de cette liste
---------------------------------------------------

Le crawler est un outil permettant de récupérer des fichiers JSON
décrivant les Chatons. Un fichier central liste les urls des JSON, et
les fichiers pointés sont aglomérés dans un unique fichier JSON.

Le modèle de description de chaque Chaton est un fichier JSON hébergé
sur son site web, valide selon un format propre au collectif. Ce modèle
a été jugé optimal car il permet à chaque Chaton de décrire exactement
son identité et ses services. Cette décentralisation des description est
une solution courante, utilisée notamment par la FFDN et les
LibreHosters.

Le choix d'héberger ce fichier sur un dépôt git
------------------------------------------------

Le choix d'utiliser un fichier central nous paraît être la meilleure
solution pour cette TX :

-   Une structure centrale existe déjà (sur le [Framagit du
    CHATONS](https://framagit.org/framasoft/CHATONS)) pour des documents
    comme la charte ou l'identité graphique du CHATONS, cela ne
    demande donc pas d'adaptation technique
-   Elle est facile à mettre en place :
    -   Pour sa lecture pour le crawler
    -   Pour les Chatons qui modifient ce fichier en ajoutant une ligne
        à chaque Chaton qui arrive
-   Elle correspond bien à une échelle moyenne d'information, ici moins
    d'une centaine de Chatons pour des fichiers légers
-   Une solution décentralisée paraît trop compliquée, et pour la PoC de
    cette TX, et de manière générale car c'est une problématique
    dépassant l'outil de recherche, mais suscitant une refonte plus
    générale de l'organisation même du CHATONS

Ces informations sont alors agglomérées dans un unique fichier JSON, et
non pas une base de donnée plus classique, pour des raisons discutées
[ci-après](export.md).

[Retour à l'accueil](index.md)
