La démarche
===========

Le [site du CHATONS](https://chatons.org) permet notamment
de recenser les hébergeurs du réseau. Ceux-ci créent des fiches pour
donner des détails sur leur structure et les services numériques qu'ils
proposent.

Malheureusement, les Chatons ne pensent pas tous à tenir leur fiche à
jour. Nous proposons ici un nouveau système pour recueillir les
informations sur les Chatons. Il ne se subsitue pas à l'ancien mais
offre une nouvelle voie pour mettre les données en ligne, en espérant
que ces deux méthodes sauront se montrer complémentaires et augmenter la
fiabilité des informations du site du CHATONS.

Le principe
===========

Il s'agit de donner les informations des Chatons de manière
décentralisée. Au lieu de remplir un formulaire sur le site du
collectif, chaque Chaton qui le souhaite peut créer un fichier JSON qui
décrit ses activités. Il dépose ensuite ce fichier JSON sur son site et
ajoute l'adresse du fichier à un répertoire commun.

Lorsque les informations du Chaton changent, avec l'ouverture d'un
nouveau service pas exemple, il suffit d'éditer ce fichier. Les
modifications seront prises en compte automatiquement, sans avoir besoin
d'aller se connecter à un autre site.

En effet, chaque nuit, la liste des fichiers à vérifier est téléchargée,
et les différents JSON sont récupérés par un programme qui vérifie
s'ils sont conformes, les agrège et les publie ici (voir plus bas).

D'autres collectifs, tels que la [FFDN](https://db.ffdn.org/) et
[LibreHosters](https://libreho.st/), utilisent un système décentralisé
similaire.

-   [Comment est gérée la liste des URL à
    vérifier](centralisation.md) (et pourquoi un dépot
    git)
-   [Comment consulter les données récoltées](export.md)
    (et pourquoi cette solution a été choisie)
-   [Comment contribuer](contribuer.md) (pour enrichir
    le schéma ou ajouter un nouveau CHATON au projet)
-   [Le détail du schéma](schema.md)
-   [Fonctionnement du crawler](crawler.md)

Obtenir les données des Chatons
===============================

Voici les liens vers les dernières données obtenues :

-   [Chercher un Chaton](http://chatons.picasoft.net/search/recherche.html)
-   [Télécharger le JSON de tous les Chatons](http://chatons.picasoft.net/exports/chatons.json)
-   [Les logs](http://chatons.picasoft.net/logs/log.txt) (pour vérifier si votre Chaton est bien
    inclus dans l'export par exemple)

Les sources
===========

Pour contribuer ou simplement regarder comment ça marche, vous pouvez
consulter notre dépôt git
[ici](https://framagit.org/bertille/tx-collecte-chatons). Il y a dedans
le schéma, la liste des URL, le code du crawler, les sources de ce site
et la documentation technique.

Ce site
=======

Son fonctionnement est expliqué [ici](site.md).
