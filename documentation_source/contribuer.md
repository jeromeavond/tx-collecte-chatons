Ajouter son Chaton
==================

Si le projet vous intéresse ou vous préférez renseigner ainsi les
informations de votre Chaton, voici la marche à suivre.

Créer le fichier JSON
---------------------

Tout d'abord, il faut renseigner les informations de votre structure.
La liste de tous les champs possibles, des valeurs qu'ils peuvent
prendre et de leur sens est
[ici](https://framagit.org/bertille/tx-collecte-chatons/-/blob/master/schema/README.md).

Vérifier que le fichier respecte bien la convention adoptée
-----------------------------------------------------------

Pour que le fichier puisse être utilisé, il faut qu'il respecte le
[schéma](http://chatons.picasoft.net/schema/schema.json). Vous pouvez vérifier que c'est bien le cas
avec un [outil de validation en
ligne](https://www.jsonschemavalidator.net/) par exemple.

Mettre le JSON en ligne
-----------------------

Assurez-vous que l'on peut avoir accès au fichier sur votre site. Vous
pouvez par exemple le déposer à la racine, avec chaton.json en nom.

Ajouter l'adresse du fichier à la liste
----------------------------------------

Vous pouvez maintenant demander à mettre à jour la liste des fichiers à
surveiller. Pour ce faire, allez sur [la
liste](https://framagit.org/bertille/tx-collecte-chatons/-/blob/master/liste/CHATONS_list.txt).

Vous devriez voir un bouton bleu sur lequel il est écrit «&nbsp;Edit&nbsp;».
Cliquez dessus. GitLab vous prévient alors que vous n'avez pas les
droits d'édition et vous propose de faire un fork.

![Bandeau d'outils pour l'édition dans
GitLab](images/edit_banner.png)

Après un petit temps de chargement, un éditeur en ligne s'ouvre. Vous
pouvez alors ajouter en bas de la liste les deux lignes suivantes :\
```
# [Nom de votre CHATON]
[URL du fichier]
```

Quand vous avez fini, cliquez sur le bouton bleu «&nbsp;Commit...&nbsp;» en
bas à gauche. Vérifiez que la case «&nbsp;Start a new merge request&nbsp;» est
bien cochée et validez en cliquant à nouveau sur le bouton, qui est
devenu vert.

La page qui s'ouvre est l'interface pour saisir une demande
d'intégration de vos modifications dans le projet principal. Donnez-lui
un titre explicite, comme par exemple «&nbsp;Ajout du Chaton X&nbsp;» et
cliquez sur«&nbsp;Submit merge request&nbsp;»

C'est fini !
-------------

Votre merge request sera bientôt validée.

Proposer une modification du schéma
===================================

Ajouter un champ
----------------

Vous trouvez qu'il manque une information importante dans le schéma ?
Vous pouvez proposer son ajout.

Ouvrez tout d'abord le
[schéma](https://framagit.org/bertille/tx-collecte-chatons/-/blob/master/schema/CHATONS_schema.json).

Réfléchissez au type de champ que vous voulez ajouter et à son
emplacement dans la hiérarchie.

Chaque champ est défini par une description, un type et des contraintes
éventuelles. Vous trouverez
[ici](https://json-schema.org/understanding-json-schema/index.html) un
tutoriel qui explique comment définir un champ. Par exemple, pour du
texte libre, on définit le champ ainsi :

```
"nouveau_champ": {
    "description": "Info très importante qui manquait",
    "type": "string"
}
```

Insérez la définition de ce nouveau champ au bon endroit. Pour une
information générale sur un Chaton, c'est sans doute dans l'objet
`"properties"`.

Pensez à modifier aussi le
[README](https://framagit.org/bertille/tx-collecte-chatons/-/blob/master/schema/README.md)
du schéma, pour documenter votre ajout.

De la même manière que pour ajouter un Chaton, faites une merge request
pour proposer vos modifications.

Ajouter un service ou un logiciel dans la liste des possibilités
----------------------------------------------------------------

Il suffit d'ajouter la valeur souhaitée dans le `enum` de la définition
de software ou de service\_type, vers la fin du schéma.

C'est la même procédure pour ajouter une valeur possible à n'importe
laquelle des listes.

[Retour à l'accueil](index.md)
