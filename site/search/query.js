// Fonction pour rendre les valeurs des champs plus user-friendly
function string_beautify(input) {
    if (input == undefined) {
        return "";
    }
    if (!input.startsWith("http")) {
        var res = input[0].toUpperCase() + input.slice(1);
        res = res.replace("_", " ");
        return res;
    }
    return input;
}

// Chargement du schéma depuis la page de recherche
function load_schema() {
    // Repérage de la frame gui contient le schéma
    var origin_frame = document.getElementById("schema");
    // Récupération du contenu de la frame
    var content = origin_frame.contentWindow.document.body.childNodes[0].innerHTML;
    // Conversion en JSON
    var schema = JSON.parse(content);

    // Génération du formulaire
    generate_form(schema);
}

// Chargement de la liste des CHATONS et peuplement de la section appropriée
function load_chatons() {
    var origin_frame = document.getElementById("chatons");
    var content = origin_frame.contentWindow.document.body.childNodes[0].innerHTML;
    var chatons = JSON.parse(content);

    const section = document.getElementById("list");
    display_chatons(chatons, section);
}

// Fonction qui permet d'afficher les informations de plusieurs Chatons dans la section voulue
function display_chatons(chatons, section) {
    // Remise à zéro de la section
    section.innerHTML = "";
    // Itération sur tous les Chatons
    for (let i = 0; i < chatons.length; i++) {
        // Création d'un conteneur
        var chaton = document.createElement("article");

        // Toutes les propriétés pertinentes
        var name = document.createElement("h3");
        name.textContent = chatons[i].name;
        chaton.appendChild(name);

        populate_article(chaton, "Site web du Chaton", chatons[i].url);
        populate_article(chaton, "Description", chatons[i].description);
        populate_article(chaton, "Structure", chatons[i].structure);
        populate_article(chaton, "Modèle économique", chatons[i].economic_model.join(", "));
        populate_article(chaton, "Public auquel s'adresse le Chaton", chatons[i].public.join(", "));

        var services = document.createElement("h4");
        services.textContent = "Liste des services proposés";
        chaton.appendChild(services);
        var services_table = document.createElement("table");
        generateTable(services_table, chatons[i].services);
        chaton.appendChild(services_table);

        populate_article(chaton, "Date de création", chatons[i].creation_date);
        populate_article(chaton, "Adresse", chatons[i].address);
        populate_article(chaton, "Périmètre d'action", chatons[i].perimeter);

        section.appendChild(chaton);
    }
}

// Une fonction pour créer plus facilement les champs des Chatons
function populate_article(article, key, value) {
    var info = document.createElement("p");
    info.textContent = key + " : " + string_beautify(value);
    article.appendChild(info);
}

// Pour générer des tableaux
function generateTable(table, data) {
    generateTableHead(table, Object.keys(data[0]));
    for (let element of data) {
        let row = table.insertRow();
        for (key in element) {
            let cell = row.insertCell();
            let text = document.createTextNode(string_beautify(element[key]));
            cell.appendChild(text);
        }
    }
}
// Avec des headers
function generateTableHead(table, data) {
    let thead = table.createTHead();
    let row = thead.insertRow();
    for (let key of data) {
        let th = document.createElement("th");
        let text = document.createTextNode(string_beautify(key));
        th.appendChild(text);
        row.appendChild(th);
    }
}

// Fonctions qui permettent d'afficher ou cacher la liste de tous les Chatons
function show_chatons() {
    const section = document.getElementById("list");
    // En affichant la section
    section.hidden = false;
    const button = document.getElementById("toggle_display");
    // Et en changeant le comportement du bouton
    button.setAttribute("value", "Cacher la liste des CHATONS");
    button.setAttribute("onclick", "hide_chatons()");
    return false;
}

function hide_chatons() {
    const section = document.getElementById("list");
    section.hidden = true;
    const button = document.getElementById("toggle_display");
    button.setAttribute("value", "Afficher tous les CHATONS");
    button.setAttribute("onclick", "show_chatons()");
    return false;
}

// Pour générer le formulaire de recherche grâce au schéma (pour connaître les avleurs possibles)
function generate_form(schema) {
    // On trouve le formulaire
    const form = document.getElementById("form");

    // Affichage de l'en-tête
    var criteria = document.createElement("h3");
    criteria.textContent = "Structure du Chaton cherché";
    form.appendChild(criteria);
    // Et génération des cases à cocher
    generate_input(true, form, "structure", schema.definitions.structure.enum);

    criteria = document.createElement("h3");
    criteria.textContent = "Services recherchés";
    form.appendChild(criteria);
    generate_input(false, form, "service", schema.definitions.service_type.enum);

    criteria = document.createElement("h3");
    criteria.textContent = "Modèle économique souhaité";
    form.appendChild(criteria);
    generate_input(false, form, "economic_model", schema.definitions.economic_model.enum);

    criteria = document.createElement("h3");
    criteria.textContent = "Publics qui peuvent utiliser le Chaton";
    form.appendChild(criteria);
    generate_input(false, form, "public", schema.definitions.public.enum);

    var submit = document.createElement("input");
    submit.setAttribute("type", "button");
    submit.setAttribute("onclick", "query()");
    submit.setAttribute("value", "Chercher");
    form.appendChild(submit);
}

// Génération des cases à cocher
function generate_input(single_valued, form, key, values) {
    // Les éléments qui vont nous servir
    var input = document.createElement("input");
    var label = document.createElement("label");
    var lb = document.createElement("br");

    // S'il n'y a qu'une seule valeur possible, on ajoute la possibilité de ne pas se prononcer
    if (single_valued) {
        // Création de l'input proprement dit
        input.setAttribute("type", "radio");
        input.setAttribute("name", key);
        input.setAttribute("value", "any");
        form.appendChild(input);

        // Et du label
        label = document.createElement("label");
        label.textContent = "Peu importe";
        form.appendChild(label);
        lb = document.createElement("br");
        form.appendChild(lb);
    }

    // Génération de tous les choix
    for (i = 0; i < values.length; i++) {
        input = document.createElement("input");
        if (single_valued) {
            input.setAttribute("type", "radio");
        } else {
            input.setAttribute("type", "checkbox");
        }
        input.setAttribute("name", key);
        input.setAttribute("value", values[i]);
        form.appendChild(input);

        label = document.createElement("label");
        label.textContent = string_beautify(values[i]);
        form.appendChild(label);
        lb = document.createElement("br");
        form.appendChild(lb);
    }
}

// Cœur de la page, fonction qui compare ce qu'il y a dans le formulaire et le json des CHATONS
function query() {
    // Afficher l'emplacement des résultats
    const section = document.getElementById("result");
    section.hidden = false;

    // Chargement des informations des CHATONS
    var origin_frame = document.getElementById("chatons");
    var content = origin_frame.contentWindow.document.body.childNodes[0].innerHTML;
    var chatons = JSON.parse(content);

    // Chargement des données du formulaire
    var elements = document.getElementById("form").elements;

    // Au départ, tous les CHATONS sont considérés comme satisfaisant potentiellement la requête
    var candidates = chatons;

    // On itère dans toutes les options du formulaire
    for (let item = 0; item < elements.length; item++) {
        // Si une valeur spéciale est demandée
        if (elements[item].checked && elements[item].value !== "any") {
            // On considère qu'aucun Chaton ne remplit les critères
            var candidates_tmp = [];
            // Et, selon le champ considéré, le traitement diffère
            if (elements[item].name === "structure") {
                for (let cand = 0; cand < candidates.length; cand++) {
                    if (candidates[cand].structure === elements[item].value) {
                        // Pour la structure, tout Chaton avec cette forme juridique passe le test
                        candidates_tmp.push(candidates[cand]);
                    }
                }
            } else if (elements[item].name === "service") {
                for (let cand = 0; cand < candidates.length; cand++) {
                    for (let serv = 0; serv < candidates[cand].services.length; serv++) {
                        if (candidates[cand].services[serv].type === elements[item].value) {
                            // Dès que le service est trouvé dans la liste des services du Chaton, on peut passer au Chaton suivant
                            candidates_tmp.push(candidates[cand]);
                            break;
                        }
                    }
                }
            } else if (elements[item].name === "economic_model") {
                for (let cand = 0; cand < candidates.length; cand++) {
                    if (candidates[cand].economic_model.includes(elements[item].value)) {
                        candidates_tmp.push(candidates[cand]);
                    }
                }
            } else if (elements[item].name === "public") {
                for (let cand = 0; cand < candidates.length; cand++) {
                    if (candidates[cand].public.includes(elements[item].value)) {
                        candidates_tmp.push(candidates[cand]);
                    }
                }
            }
            // Pour examiner le prochain critère, on part de la liste des Chatons qui ont validé cette étape
            candidates = candidates_tmp;
        }
    }

    // On change l'affichage de la section des résultats pour montrer les Chatons trouvés
    const result_section = document.getElementById("candidates");
    display_chatons(candidates, result_section);

    return false;
}
